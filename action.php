<?php

require_once './models/newclientsubmitdata.php';
require_once './posthandleuploadfiles/handleuploadfiles.php';
//image/png, image/gif, image/jpg, image/jpeg, image/*,.pdf
$fileuploadtype = $_FILES['identification']['type'];
echo $fileuploadtype;
if($fileuploadtype != "image/png" && $fileuploadtype != "image/gif" &&
   $fileuploadtype != "image/jpg" && $fileuploadtype != "image/jpeg" &&
   $fileuploadtype != "image/.pdf" && $fileuploadtype != "application/pdf") {
    echo $fileuploadtype . " - Only the following image types are allowed!:<br>image/png, image/gif, image/jpg, image/jpeg, image/*,.pdf";
    if (isset($_SERVER["HTTP_REFERER"])) { header("Location: " . $_SERVER["HTTP_REFERER"]); }
}
else
{
    echo "ALLOWED: " .  $fileuploadtype;
    // exit;
}

$newdata = new NewClientSubmitData(
    $_POST['fname']   ?? "n/a",
    $_POST['lname']   ?? "n/a",
    $_POST['email']   ?? "n/a",
    $_POST['phone']   ?? "n/a",
    $_POST['billing'] ?? "n/a",
    $_POST['servicesplan1'] ?? "n/a",
);

$imagesData = new HandleFileUpload(
    $_FILES["identification"]["tmp_name"],
    $_FILES["identification"]["name"],
    $_FILES['identification']['size'],
    $_FILES["proofofincome"]["tmp_name"],
    $_FILES["proofofincome"]["name"],
    $_FILES['proofofincome']['size'],
    $_FILES["proofofaddress"]["tmp_name"],
    $_FILES["proofofaddress"]["name"],
    $_FILES['proofofaddress']['size']
);

$newdata->ToString();
echo "<br>";
echo "<img src='". $imagesData->identification_imgdir . "' alt='uploaded image identifaction' width=500 />";
echo "<br>";
echo "<img src='". $imagesData->proofofincome_imgdir . "' alt='uploaded image proofofincome' width=500 />";
echo "<br>";
echo "<img src='". $imagesData->proofofaddress_imgdir . "' alt='uploaded image proofofaddress' width=500 />";
?>