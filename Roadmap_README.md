# ROADMAP


**Description**  
This ROADMAP README contains a list of future features.

-------------------------------------------------------

**Stakeholders**  
Department: Customer Interfaces & Development.

-------------------------------------------------------

## New Features  
Features can change based on input of the current stakeholders.


### Make form editable for existing subscribers  
For now the form will be launched for new users only.
When all is working, the form can also be used for existings users to change their data and subscription.


### Display ratings
Users like to get information about the channels or packages they are going to pay for.
Ratings give a sense of knowing what to expect before they purchase it.

source: _https://www.pricespider.com/why-your-customers-want-to-see-ratings-and-reviews/_  
Ratings and reviews are crucial signals consumers use to determine a product's quality. They provide “social proof,” 
letting people see what previous customers have thought after purchasing the product.

### Display images for a packages/channels
source: _https://www.toptal.com/designers/ux/user-experience-imagery_


### Automate data capture to save data directly in CCM  
For now CSR's will receive e-mails and handle the rest of the work to create a subscription for the subscriber.
In the future some aspects can be automated, depending on the level of complexity.

### Automate order to create an invoice for faster signing.
For now new subscribers will press order which sends an e-mail to a setar CSR which then wil start
sending mails to get all the necessary information.
In the future the subscriber can receive his invoice directly from the webpage itself 
or automatically by e-mail which print out, sign, scan and send back.
Only when the subscriber does not have a printer, he/she can come to a teleshop to give his/her signature.