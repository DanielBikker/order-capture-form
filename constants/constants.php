<?php

class Constants {
    const AuthorizationBearer = "Authorization: Bearer ";
    const Hd = "hd";
    const NonRecurring = 1;
    const Recurring = 2;
    const Residential = 'residential';
    const Additional = 'additional';// additionals zijn voor appartementen? (goedkoper) deze mogen niet getoond worden in de store?
    const api2url_getservicetype30 = 'https://api2.setar.aw/customercaredb/rest/aw.setar.customercare.rest.entities.servicetype/30';
    const api2url_getservicetype33 = 'https://api2.setar.aw/customercaredb/rest/aw.setar.customercare.rest.entities.servicetype/33';
}
?>