<?php
include "./constants/constants.php";
include "./models/servicetype.php";

class Packages {

    public static $residentialServicesList = null;
    public static $additionalServicesList = array();

    function GetPackages()
    {
        include "getjwt.php";
        $constants = new Constants();
        
        ///GET PACKAGES
        
        $cURLConnection2 = curl_init();
        $authorization = $constants::AuthorizationBearer . $jsonResponse->access_token;
        curl_setopt($cURLConnection2, CURLOPT_URL, $constants::api2url_getservicetype30);
        curl_setopt($cURLConnection2, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cURLConnection2, CURLOPT_POST, 0);
        curl_setopt($cURLConnection2, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
        
        $servicesResult = curl_exec($cURLConnection2);
        $jsonResponse2 = json_decode($servicesResult,true);
        
        self::$residentialServicesList = array();
        self::$additionalServicesList = array();
        foreach($jsonResponse2['servicePackage'] as $servicePackage)
        {
            $serviceTitle = $servicePackage['title'];

            // Add residential packages to residentialServicesList.
            if(strpos(strtolower($serviceTitle), $constants::Residential) !== false)
            {
                $stringArray = explode(" ", $serviceTitle);
                if(strtolower($stringArray[count($stringArray)-1]) == $constants::Hd)
                {// item contains HD and could be a upgrade if before this item came SD.
                    // Deze service worden gecombineerd naar 1 (sd + hd als upgrade).
                    if(count(self::$residentialServicesList) > 0)
                    {
                        if(ServiceType::IfIsUpgrade_MergeWithDefaultService(self::$residentialServicesList, $servicePackage))
                        {
                            continue;
                        }
                    }
                }
                else
                {
                    $service = new ServiceType($servicePackage);
                    array_push(self::$residentialServicesList, $service);        
                }
            }
            else
            { // Add addition and other packages to additionalServicesList.
                //echo "<br><br> NOT RESIDENTIAL and does NOT contain HD!";
                $service = new ServiceType($servicePackage);
                array_push(self::$additionalServicesList, $service);
            }
        }
        
        curl_close($cURLConnection2);
        ServiceType::SetDisplayNames(self::$residentialServicesList);
        //return $residentialServicesList;
    }
}
?>