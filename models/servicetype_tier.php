<?php

/**
    Example of tiers: Basic Sport, Basic Health Living, Basic Learning, Basic Latin, Basic Music.
 */

    class ServiceType_Tier {
        public $id = 0;
        public $title = "";
        public $monthlyPrice = 0;

        public function __construct($id_, $title_, $monthlyPrice_)
        {
            $this->id = $id_;
            $this->title = $title_;
            $this->monthlyPrice = $monthlyPrice_;
        }
    }
?>