<?php

/*
    This class contains all order information the subscriber has selected.

    See composition here https://www.setar.aw/residential/internet-fixed-tv/tv/compose-2/
*/

    include './servicetype.php';


    class OrderCapture {

        public $serviceType = "";
        public $billableDeposit_SDHD_Equipment = 0;
        public $billableDeposit_DVR_Equipment = 0;
        public $basicTiers = array();
        public $premiumTiers = array();

        //Additional equipment rentals
        public $SDBox_rental_amount = 0;
        public $HDBox_rental_amount = 0;
        public $DVRBox_rental_amount = 0;

        //Extra packages
        
        

        
        public function __construct(ServiceType $serviceType_, $sdhd_equipment_amount, $dvr_equipment_amount)
        {
            $this->serviceType = $serviceType_;

        }
        
    }

?>