<?php

// include './../helper/servicetypehelper.php';

/*
    Examples of service types: Digital Basic, Basic Mix,...,Supreme.

    The api2.setar.aw calls this a servicetype.

    See composition here https://www.setar.aw/residential/internet-fixed-tv/tv/compose-2/
*/
class ServiceType
{
    
    public $id = 0;
    public $upgrade_id = 0;
    public $title = "";
    public $displayTitle = "";
    public $upgradeTitle = "";
    public $displayUpgradeTitle = "";
    public $isSD = false;
    public $isHD = false;
    public $includes = "";

    public $amountOfBasicTiersToChoose = 0; //all is max = 5 tiers.
    public $amountOfPremiumTiersToChoose = 0; // al is max = 4 tiers
    public $hasOneFreeHDBox = false;

    public $hasUpgrade = false;
    public $hasUpgradeToDVR = false;

    public $monthlyPrice = 0;
    public $upgradeMonthlyPrice = 0;

    public $listOrder = 0;
    public $listOrderUpgrade = 0;

    public static $instance;

    public function __construct(array $response = null) {//, bool $hasUpgrade_, bool $isSD_, bool $isHD, bool $hasUpgradeToDVR_, float $upgradeMonthlyPrice_)//($id_, $title, $hasUpgrade_, $monthlyPrice_, $listOrder_)
        self::$instance = $this;

        if(isset($response))
        {
            $this->id = $response["id"];
            $this->title = $response["title"];
            $this->monthlyPrice = $response["monthlyPrice"];
            $this->listOrder = $response["listOrder"];
        }
    }

    public static function getInstance() {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /*
        If current item title contains HD.
        Check if previous item contains SD (in order of json result (see getpackages)).
        If item exists then merge into one item.
    */
    public static function IfIsUpgrade_MergeWithDefaultService(&$servicesList, $currentServiceItem) {
        $defaultServiceItem = self::GetPreviousItemFromListById($servicesList, $currentServiceItem["id"]);

        if ($defaultServiceItem != null) //if item exists, then current item is an upgrade
        {
            self::MergeServiceWithUpgradeService($servicesList, $defaultServiceItem, $currentServiceItem);
            return true;
        }
        return false;
    }

    /*
        Merge upgrade with default service e.g.:
        Residential Basic SD with Residential Basic HD for the user interface to have an upgrade option within the same object.
    */
    static function MergeServiceWithUpgradeService(&$servicesList, ServiceType $defaultServiceItem, array $upgradeItem) {
        // Merge upgrade item with default item
        $defaultServiceItem->upgrade_id = $upgradeItem["id"];
        $defaultServiceItem->upgradeTitle = $upgradeItem["title"];
        $defaultServiceItem->upgradeMonthlyPrice = $upgradeItem["monthlyPrice"];
        $defaultServiceItem->listOrderUpgrade = $upgradeItem["listOrder"];
        $defaultServiceItem->isHD = true;
        $defaultServiceItem->hasUpgrade = true;

        // Remove item from foreach.
        foreach ($servicesList as $index => $value) {
            if ($value->id == $defaultServiceItem->id) {
                array_splice($servicesList, $index, 1);// remove item, to prevent duplicate in array.
            }
        }

        array_push($servicesList, $defaultServiceItem);
    }

    public static function SetDisplayNames(&$servicesList) {
        foreach ($servicesList as $item) {
            $strArray = explode(" ", $item->title);
            $strArrayUpgrade = explode(" ", $item->upgradeTitle);
            if ($item->hasUpgrade) {
              if (strtolower($strArray[count($strArray) - 1]) == "sd") {
                array_splice($strArray, (count($strArray) - 1), 1);
              }

              array_splice($strArrayUpgrade, 0, 2);
              $item->upgradeTitle = implode(" ", $strArrayUpgrade);
            }
            array_splice($strArray, 0, 2);

            $item->title = implode(" ", $strArray);
            self::SetIncludeTextsPerServicePackage_HARDCODED($item);
          }
    }

    /*
        Return previous item if title contains SD.
    */
    static function GetPreviousItemFromListById($list, $idvalue) {
        foreach ($list as $item) {
            if ($item->id == ($idvalue - 1)) {
                $strArray = explode(" ", $item->title);
                if (strtolower($strArray[count($strArray) - 1]) == "sd") {
                    return $item;
                } else {
                    continue;
                }
            }
        }

        return null;
    }

    /*
        Set package includes for each package e.g.: Includes: Digital Basic and 2 Basic Tiers....
    */
    static function SetIncludeTextsPerServicePackage_HARDCODED(&$packageItem) {
        switch ($packageItem->title) {
            case "Basic Mix":
                $packageItem->includes = "Includes: Digital Basic and 2 Basic Tiers";
                break;
            case "Basic Mix Plus One":
                $packageItem->includes = "Includes: Digital Basic, 2 Basic Tiers, 1 Premium Tier and 1 High Definition Box";
                break;
            case "Premium Mix":
                $packageItem->includes = "Includes: Digital Basic, 2 Premium Tier and 1 High Definition Box";
                break;
            case "Fusion":
                $packageItem->includes = "Includes: Digital Basic, 2 Basic Tiers, Sports Premium, HBO Premium, STAR Premium and 1 High Definition Box";
                break;
            case "Supreme":
                $packageItem->includes =  "Includes: Digital Basic, all Basic Tiers, all Premium Tiers";
                break;
            default:
                $packageItem->includes = "";
        }
        
    }
}
