<?php 
    class NewClientSubmitData {
        public $fname;
        public $lname;
        public $email;
        public $phone;
        public $billing;
        public $id_imgdir;
        public $servicesplanchoice;

        public function __construct(string $firstname, string $lastname, string $email, string $phone, string $billingaddress, 
        // string $filetempname, string $filename, string $filesize,
        string $servicesplan)
        {
            //Fix mysql db connection first
            // mysql_real_escape_string() Call to undefined function mysql_real_escape_string()
            $this->fname = $firstname == "" ? "Warning: field empty" : $firstname;
            $this->lname = $lastname == "" ? "Warning: field empty" : $lastname;
            $this->email = $email == "" ? "Warning: field empty" : $email;
            $this->phone = $phone == "" ? "Warning: field empty" : $phone;
            $this->billing = $billingaddress == "" ? "Warning: field empty (must provide)" : $billingaddress;
            $this->servicesplanchoice = $servicesplan == "" ? "Warning: field empty" : $servicesplan;

            //$this->HandleUploadedFile($filetempname,$filename, $filesize);
        }

        public function ToString()
        {
            echo "Firstname: " . $this->fname . "<br>";
            echo "Lastname: " . $this->lname . "<br>";
            echo "E-mailaddress: " . $this->email . "<br>";
            echo "Phone No: " . $this->phone . "<br>";
            echo "Billing address: " . $this->billing . "<br>";
            echo "Service plan: " .  $this->servicesplanchoice . "<br>";
        }
    }
?>