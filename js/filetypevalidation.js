$(document).ready(function(){
    // $('#mymodal').on('shown.bs.modal', function () {
    //     $('#myInput').trigger('focus')
    // });
    var myModal = document.getElementById('mymodal')
    var myInput = document.getElementById('myInput')
    
    //  myModal.addEventListener('shown.bs.modal', function () {
    //      alert("test");
    //      //myInput.focus()
    //  })

    $("#closemodal").on("click", function(){
        $("#mymodal").hide();
    });

    $('input[type=file]').change(function () {
        
        var val = $(this).val().toLowerCase(),
            regex = new RegExp("(.*?)\.(pdf|bmp|ppt|png|jpg|jpeg|gif)$");

        if (!(regex.test(val))) {
            $(this).val('');
            $("#mymodal").show();
        }
    });
})