var prefix_fname = "First name: ";
var prefix_lname = "Last name: ";
var prefix_email = "E-mail address: ";
var prefix_phone = "Phone nr: ";
var prefix_billing = "Billing address: ";
var prefix_servicesplan = "Services Plan: ";
var prefix_additional = "Additional packages: ";

var firstname ="";
var lastname ="";
var emailaddress ="";
var billingaddress ="";
var phonenr ="";
var service ="";
var additionalpackage ="";

$(document).ready(function(){

    var allinput = document.getElementsByTagName("input");
    for(var i = 0; i < allinput.length; i++)
    {
        allinput[i].addEventListener('focusout', (event) => {
            validate(event.target);
        });
    }
    
    document.getElementById("email").addEventListener('oninput', (event) => {
        handleEmail(event.target);
    });

    document.getElementsByTagName('body')[0].addEventListener('onkeydown', (event) => {
      console.log(event.keyCode);
      if(event.keyCode == 13){
         //....your actions for the keys .....
          console.log("enter pressed");
      }
    });
});

function handleEmail(emailInput)
{
    emailInput.value = emailInput.value.replace(/^.$.[^a-zA-Z0-9.@-_]/g, '');
    saveFieldData('eamil');
}

function saveFieldData(field)
{
    if(field =="fname")
    {
        var firstnameobject = document.getElementById("fname");
        if(firstnameobject != undefined && firstnameobject != null)
        {
            document.getElementById("sum_fname").innerHTML = prefix_fname + firstnameobject.value;
        }
    }

    if(field =="lname")
    {
        var lastnameobject = document.getElementById("lname");
        if(lastnameobject != undefined && lastnameobject != null)
        {
            if(lastnameobject.value != "")
            {
                document.getElementById("sum_lname").innerHTML = prefix_lname + lastnameobject.value;
            }
        }
    }

    if(field == "email")
    {
        var emailobject = document.getElementById("email");
        if(emailobject != undefined && emailobject != null)
        {
            if(emailobject.value != "")
            {
                document.getElementById("sum_email").innerHTML = prefix_email + emailobject.value;
            }
        }
    }

    if(field == "phone")
    {
        var phoneobject = document.getElementById("phone");
        if(phoneobject != undefined && phoneobject != null)
        {
            document.getElementById("sum_phone").innerHTML = prefix_phone + phoneobject.value;
        }
    }
    
    if(field =="billing")
    {
        var billingaddressobject = document.getElementById("billing");
        if(billingaddressobject != undefined && billingaddressobject != null)
        {
            document.getElementById("sum_billingaddress").innerHTML = prefix_billing + billingaddressobject.value;
        }
    }

    if(field =="servicesplan1")
    {
        var servicesplanobject = document.getElementById("servicesplan1");
        if(servicesplanobject != undefined && servicesplanobject != null)
        {
            document.getElementById("sum_serviceplan").innerHTML = prefix_servicesplan + servicesplanobject.value;
        }
    }

    if(field =="additional")
    {
        var additionalobject = document.getElementById("additional");
        if(additionalobject != undefined && additionalobject != null)
        {
            document.getElementById("sum_additional").innerHTML = prefix_additional + additionalobject.value;
        }
    }
}

var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab, null); // Display the current tab

/*
    showTab is called when clicking the next or prev button (nextPrev())
*/
function showTab(n, isNext) {
  // This function will display the specified tab of the form...
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";
  //... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }
  if (n == (x.length - 1)) {
    document.getElementById("nextBtn").innerHTML = "Submit";
  } else {
    document.getElementById("nextBtn").innerHTML = "Next &nbsp;<span class='glyphicon glyphicon-chevron-right'></span>";
  }
  //... and run a function that will display the correct step indicator:
  fixStepIndicator(n, isNext);
}

/*
    Display the previous or next slide and set the submit button for the last page.
*/
function nextPrev(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tab");
  // Exit the function if any field in the current tab is invalid:
  //if (n == 1 && !validateForm()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;
  // if you have reached the end of the form...
  if (currentTab >= x.length) {
    // ... the form gets submitted:
    document.getElementById("regForm").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTab(currentTab, n == 1);
}

/* 
 Validate current input field.
*/
function validate(input) {
    // This function deals with validation of the form fields
    if(input.classList.length > 0)
    {
        if(input.classList[0].includes("invalid"))
        {
            input.classList.remove("invalid")
        }
    }
}

/* 
 Validate form when pressing Next button.
*/
function validateForm() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].getElementsByTagName("input");
  // A loop that checks every input field in the current tab:
  for (i = 0; i < y.length; i++) {
    // If a field is empty...
    if (y[i].value == "") {
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false
      valid = false;
    }
    else
    {
        y[i].classList.remove("invalid");
    }
  }
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }

  //return true;//DANIEL TESTCODE REMOVE!
  return valid; // return the valid status
}

function fixStepIndicator(n, isNext) {
    // This function removes the "active" class of all steps...
    var i, x = $(".progressbar > li");
    var currentTabIndex = n;
    if(isNext !== null && isNext !== undefined)
    {
        if(!isNext)
        {
            if(currentTabIndex <= (x.length-1) && x[n+1] != undefined)
            {
                x[n+1].className = x[n+1].className.replace("active", "");
            }
        }
    }

    if(currentTabIndex <= (x.length-1) && currentTabIndex != -1)
    {
        // add "active" class on the current progress step:
        x[currentTabIndex].className = "active";
    }
}
  