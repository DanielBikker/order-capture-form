$(document).ready(function () {

    SetCurrentDigitalServicePlan();
    $(".cableplansbtn").on("click", OnCablePlansBtnChanged);
    $("#boxtype").on("change", OnBoxTypeSelectChanged);

    // console.log(residentialServicesList[0]);

});

function OnCablePlansBtnChanged(evt) {
    var btnprimary = $(".setarselectedbtn");
    btnprimary.addClass("btn-light");
    btnprimary.removeClass("setarselectedbtn");
    var clickedBtn = $(evt.target);
    clickedBtn.addClass("setarselectedbtn");
    clickedBtn.removeClass("btn-light");
    console.log(clickedBtn);

    $("#cableplansloader").removeClass("hide");
    setTimeout(function () {
        $("#cableplansloader").addClass("hide");
    }, 700);

    SetCurrentDigitalServicePlan();
}

function OnBoxTypeSelectChanged(evt) {
    // console.log(evt.target);
    SetDisplayPrice(evt.target.value);
}

function SetCurrentDigitalServicePlan() {
    var selectedCablePlanTitle = $(".setarselectedbtn").text();
    $(".card-header")[0].innerHTML = selectedCablePlanTitle;

    setCurrentServiceDisplay($(".setarselectedbtn")[0].id);
}

function setCurrentServiceDisplay(id) {
    for (var i = 0; i < residentialServicesList.length; i++) {
        var servicePlanItem = residentialServicesList[i];
        if (servicePlanItem.id == id) {
            console.log(servicePlanItem);
            SetDisplayPrice(servicePlanItem.monthlyPrice);
            $(".card-includes-text")[0].innerHTML = servicePlanItem.includes;

            if($("#boxtype")[0].children.length > 0)
            {
                $("#boxtype")[0].innerHTML = "";
            }
            if (servicePlanItem.hasUpgrade && $("#boxtype")[0].children.length == 0) {
                // Populate <select></select>
                var option = document.createElement("option");
                option.value = servicePlanItem.monthlyPrice;
                option.innerHTML = "Standard Definition Box";
                var option2 = document.createElement("option");
                option2.value = servicePlanItem.upgradeMonthlyPrice;
                option2.innerHTML = "High Definition Box";
                $("#boxtype")[0].appendChild(option);
                $("#boxtype")[0].appendChild(option2);
                $("#boxtype").removeClass("hide");
            }
            else
            {
                $("#boxtype").addClass("hide");
            }
        }
    }
}

function SetDisplayPrice(price)
{
    $(".pricedisplay").html("Price " + price + ",- fl");
}