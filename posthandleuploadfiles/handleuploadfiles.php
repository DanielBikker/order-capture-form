<?php
class HandleFileUpload
{
    public $identification_imgdir;
    public $proofofincome_imgdir;
    public $proofofaddress_imgdir;


    public function __construct(
        string $filetempname_identification,
        string $filename_identification,
        string $filesize_identification,

        string $filetempname_proofofincome,
        string $filename_proofofincome,
        string $filesize_proofofincome,

        string $filetempname_proofofaddress,
        string $filename_proofofaddress,
        string $filesize_proofofaddress
    ) {
        //Fix mysql db connection first
        // mysql_real_escape_string() Call to undefined function mysql_real_escape_string()
        $this->HandleUploadedFile_ForIdentification($filetempname_identification, $filename_identification, $filesize_identification);
        $this->HandleUploadedFile_ForProofOfIncome($filetempname_proofofincome, $filename_proofofincome, $filesize_proofofincome);
        $this->HandleUploadedFile_ForProofOfAddress($filetempname_proofofaddress, $filename_proofofaddress, $filesize_proofofaddress);
    }

    function HandleUploadedFile_ForIdentification(string $filetempname, string $filename, string $filesize)
    {
        $uploads_dir = 'uploads';
        $filesize_mb = intval($filesize) / 1024 / 1024; //($files['identification']['size'] / 1024 / 1024);
        if ($filesize_mb > 1) {
            echo "file too large kb = " . $filesize_mb . "<br>";
            echo "file can not exceed 16 kb";
            return;
        }

        move_uploaded_file($filetempname, "$uploads_dir/$filename");
        $this->identification_imgdir = $uploads_dir . "/" . $filename;
    }

    function HandleUploadedFile_ForProofOfIncome(string $filetempname, string $filename, string $filesize)
    {
        $uploads_dir = 'uploads';
        $filesize_mb = intval($filesize) / 1024 / 1024; //($files['identification']['size'] / 1024 / 1024);
        if ($filesize_mb > 1) {
            echo "file too large kb = " . $filesize_mb . "<br>";
            echo "file can not exceed 16 kb";
            return;
        }

        move_uploaded_file($filetempname, "$uploads_dir/$filename");
        $this->proofofincome_imgdir = $uploads_dir . "/" . $filename;
    }

    function HandleUploadedFile_ForProofOfAddress(string $filetempname, string $filename, string $filesize)
    {
        $uploads_dir = 'uploads';
        $filesize_mb = intval($filesize) / 1024 / 1024; //($files['identification']['size'] / 1024 / 1024);
        $maxfilesizein_mb = 1;
        if ($filesize_mb > 1) {
            echo "file too large kb = " . $filesize_mb . "<br>";
            echo "file can not exceed 16 kb";
            return;
        }

        move_uploaded_file($filetempname, "$uploads_dir/$filename");
        $this->proofofaddress_imgdir = $uploads_dir . "/" . $filename;
    }
}
