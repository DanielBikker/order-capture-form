<?php
include 'getpackages.php';
$packagesService = new Packages();
$packagesService->GetPackages();
// echo "<br>Additional: <br><br>";
// var_dump($packagesService::$additionalServicesList);
// echo "<br>Residential: <br><br>";
// var_dump($packagesService::$residentialServicesList);

echo "<br><br>";

?>

<!DOCTYPE html>
<html>

<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet">
  <link href="css/themecolors.css" rel="stylesheet">
  <link href="css/progressbtns.css" rel="stylesheet">

  <!-- BOOTSTRAP VERSION 3.4 -->
  <link rel="stylesheet" href="bootstrap5/bootstrap5_0_2.min.css">
  <script src="bootstrap5/bootstrap5_0_2.bundle.min.js" crossorigin="anonymous"></script>

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  <script>
    <?php
    $php_array = $packagesService::$residentialServicesList;
    $js_array = json_encode($php_array);
    echo "var residentialServicesList = " . $js_array . ";\n";
    ?>

    // console.log(residentialServicesList);
  </script>
</head>



<body>

  <h3 class="setarthemegreen_labelcolor">Registration CABLE TV</h3>

  <form id="regForm" action="action.php" method="post" enctype="multipart/form-data">

    <!-- One "tab" for each step in the form: -->
    <div class="tab">
      <div class="tab-tab1">
        <h4><span class="stepnr"> 1.</span> Personal information</h4>
        <div class="mb-3 fnamegroup">
          <label for="fname" class="form-label">First name</label>
          <input class="form-control" id="fname" name="fname" placeholder="First name..." required oninput="saveFieldData('fname')">
        </div>

        <div class="mb-3 lnamegroup">
          <label for="fname" class="form-label">Last name</label>
          <input class="form-control" id="lname" name="lname" placeholder="Last name..." required oninput="saveFieldData('lname')">
        </div>

        <div class="mb-3 emailgroup">
          <label for="email" class="form-label">E-mail address</label>
          <input class="form-control" id="email" name="email" placeholder="E-mail..." required oninput="saveFieldData('fname')">
        </div>

        <div class="mb-3 phonegroup">
          <label for="phone" class="form-label">Phone</label>
          <input type="tel" id="phone" name="phone" class="form-control" placeholder="Phone..." maxlength="20" required oninput="this.value = this.value.replace(/[^0-9\.\-()]/g, '');saveFieldData('phone')">
        </div>

        <div class="mb-3 billinggroup">
          <label for="billing" class="form-label">Billing address</label>
          <input class="form-control" id="billing" name="billing" placeholder="Billing address..." oninput="saveFieldData('billing')" required oninput="saveFieldData('fname')">
        </div>

        <div class="mb-3 uploadgroup_id">
          <label for="identification" class="form-label" title="png/gif/jpeg/pdf">Id or passport</label>
          <input type="file" class="form-control" id="identification" name="identification" required>
          <!-- accept="image/png, image/gif, image/jpg, image/jpeg, image/*,.pdf" -->
        </div>

        <div class="mb-3 uploadgroup_income">
          <label for="proofofincome" class="form-label" title="png/gif/jpeg/pdf">Proof of income</label>
          <input type="file" class="form-control" id="proofofincome" name="proofofincome" required>
          <!-- accept="image/png, image/gif, image/jpg, image/jpeg, image/*,.pdf" -->
        </div>

        <div class="mb-3 uploadgroup_address">
          <label for="proofofaddress" class="form-label" title="png/gif/jpeg/pdf">Proof of address</label>
          <input type="file" class="form-control" id="proofofaddress" name="proofofaddress" required>
          <!-- accept="image/png, image/gif, image/jpg, image/jpeg, image/*,.pdf" -->
        </div>
      </div>
    </div>

    <!-- <div class="tab">
      <div class="tab-tab2">
        <h4><span class="stepnr"> 2.</span> Choose your digital service plan</h4>
        <div class="form-group serviceplan">
          <select class="form-control" id="servicesplan1" name="servicesplan1" oninput="saveFieldData('servicesplan1')" required>
            <option selected>Choose</option> -->
    <?php

    // foreach ($packagesService::$residentialServicesList as $item) {
    //   $title = $item->title;
    //   echo "<option value='" . $item->id . "'>" . $title . "</option>";
    // }

    ?>
    <!-- <option value="digitalbasic">Digital Basic</option>
          <option value="premiummix">Premium Mix</option> -->
    <!-- </select>
        </div>

      </div>
    </div> -->



    <div class="tab">
      <!-- <h2><span class="stepnr">3.</span> testing</h2> -->

      <h4><span class="stepnr">2.</span> Choose your service plan</h4>

      <div class="cableplansbtnscontainer">
        <br>
        <div class="allprogressbtnscontainer">
          <?php
          $length = count($packagesService::$residentialServicesList);
          foreach ($packagesService::$residentialServicesList as $key => $value) {
            $percentage = ((100 / $length) * ($key + 1));
            $percentage = $percentage == 100 ? $percentage -= 1 : $percentage; // progressbar 100% width sticks out from its button above. 
            $item = $packagesService::$residentialServicesList[$key];
            $title = $item->title;
            // if ($key == 0) {
            //   echo "<button id='". $item->id ."' data-index='". $key ."' data-percentage='". ((100 / $length)*($key+1))."' type='button' class='btn setarselectedbtn btnmargins cableplansbtn'>" . $title . "</button>";
            // } else {
            //   echo "<button id='". $item->id ."' data-index='". $key ."' data-percentage='". ((100 / $length)*($key+1))."' type='button' class='btn btn-light btnmargins cableplansbtn'>" . $title . "</button>";
            // }         

            if ($key == 0) {
              echo "<div class='progressbtncontainer'>
                      <button id='" . $item->id . "' data-index='" . $key . "' data-percentage='" . $percentage . "' type='button' class='btn setarselectedbtn btnmargins cableplansbtn'>" . $title . "</button>
                      <div class='progressdiv'>
                          <div class='progress-bar progress-bar-striped' role='progressbar' style='width: " . $percentage . "%' aria-valuenow='25' aria-valuemin='0' aria-valuemax='100'></div>
                      </div>
                    </div>";
            } else {
              echo "<div class='progressbtncontainer'>
                      <button id='" . $item->id . "' data-index='" . $key . "' data-percentage='" . $percentage . "' type='button' class='btn btn-light btnmargins cableplansbtn'>" . $title . "</button>
                      <div class='progressdiv'>
                          <div class='progress-bar progress-bar-striped' role='progressbar' style='width: " . $percentage . "%' aria-valuenow='25' aria-valuemin='0' aria-valuemax='100'></div>
                      </div>
                    </div>";
            }
          }
          ?>
        </div>
      </div>

      <div id="cableplansloader" class="spinner-border text-info hide" role="status">
        <span class="visually-hidden">Loading...</span>
      </div>

      <div class="card text-center cableplanscard">
        <div class="card-header">
          Featured
        </div>
        <div class="card-body">
          <h5 class="card-title"></h5>
          <p class="card-includes-text">Includes: </p>
          <select class="form-control selectboxtype" id="boxtype">
            <!-- <option>Standard Definition Box</option>
            <option>High Definition Box</option> -->
          </select>
          <a href="#" class="btn setarbtnprimary">Order now</a>
        </div>
        <div class="card-footer highlightedgreen pricedisplay">

        </div>
      </div>
    </div>


    <div class="tab">
      <h2><span class="stepnr">4.</span> Additional packages</h2>

      <div class="input-and-label-container">
        <div>
          <input class="form-check-input" aria-label="" type="checkbox" name="indian" id="indian" value="indian">
        </div>
        <div>
          <label for="indian" oninput="saveFieldData('additional')">Indian Total Package</label>
        </div>
      </div>
    </div>

    <div class="tab">
      <h2><span class="stepnr">5.</span>Summary</h2>
      <p id="sum_fname">First name: </p>
      <p id="sum_lname">Last name: </p>
      <p id="sum_email">E-mail address: </p>
      <p id="sum_phone">Phone No: </p>
      <p id="sum_billingaddress">Billing address: </p>
      <br>
      <p id="sum_serviceplan">Service Plan: </p>
      <p id="sum_additional">Additional packages: </p>
      <br></br>
      <img src="imgs/recaptcha_demo.png" width="300" />
    </div>

    <div class="bottomelements-container">
      <div class="btncontainer" style="float:right;">
        <button class="btn btn-sm btntextcolor" type="button" id="prevBtn" onclick="nextPrev(-1)">
          <span class="glyphicon glyphicon-chevron-left"></span>&nbsp; Previous
        </button>

        <button class="setarthemegreen_btnprimary btn btn-sm" type="button" id="nextBtn" onclick="nextPrev(1)">
          Next &nbsp;<span class="glyphicon glyphicon-chevron-right"></span>
        </button>
      </div>

      <div class="steps-progressbar-container">
        <ul class="progressbar">
          <li class="active"></li>
          <li></li>
          <li></li>
        </ul>
      </div>
    </div>
  </form>

  <div id="mymodal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Image type not allowed</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p>Image must be of type: pdf, bmp, ppt, png, jpg, jpeg, gif.</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
          <button id="closemodal" type="button" class="setarthemegreen_btnprimary btn btn-secondary" data-bs-dismiss="mymodal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->

  <script src="js/jquery_3.6.0.min.js"></script>
  <script src="js/formfunctions.js"></script>
  <script src="js/filetypevalidation.js"></script>
  <script src="js/cableplansbuttons_andcard_logic.js"></script>
</body>

</html>